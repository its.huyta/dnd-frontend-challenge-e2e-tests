# DnD Frontend Challenge E2E Testing

## Instructions

- First, install the dependencies:

```bash
yarn
npx playwright install
```

- To run all the E2E tests in all the browsers specified in the `playwright.config.ts` file:

```bash
yarn test
```

- To run all the E2E tests in headed browers:

```bash
npx playwright test --headed
```

## Environment variables

- All the environment variables will be read from `.env`. 

| Variable name | Usage | Default |
| ------------- | ----- | ------- |
| APP_BASE_URL | The base URL of the DnD Spells application | http://localhost:5173 |
