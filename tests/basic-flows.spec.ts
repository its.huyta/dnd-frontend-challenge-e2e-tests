import { test } from '@playwright/test';
import { navigateToSpellListPage } from './common/navigate';
import { markFavoriteFirstSpell, checkSpellInFavoriteList } from './steps/favorite.steps';

test.describe('Basic flows', () => {
  test('List spells and mark the first one as favorite', async ({ page }) => {
    await navigateToSpellListPage({ page });

    const spellIndex = await markFavoriteFirstSpell({ page });

    await checkSpellInFavoriteList({ page, spellIndex });
  });
});
