import { Page } from "@playwright/test";
import { navigateToFavoritePage } from "../common/navigate";
import { waitForFirstSpell, waitForSpell } from "./spell.steps";

export const getSpellFavoriteButtonSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-favorite-button"]`;

  return `[data-testid="spell-favorite-button-${spellIndex}"]`;
};

export const markFavoriteFirstSpell = async ({ page }: { page: Page }) => {
  const spellLocator = await waitForFirstSpell({ page });
  const spellIndex = await spellLocator.getAttribute('id') as string;

  const addToFavoriteButton = spellLocator.locator(getSpellFavoriteButtonSelector(spellIndex));
  await addToFavoriteButton.click();
  await addToFavoriteButton.locator('[data-testid="FavoriteIcon"]').waitFor();

  return spellIndex;
};

export const checkSpellInFavoriteList = async ({ page, spellIndex }: { page: Page, spellIndex: string }) => {
  await navigateToFavoritePage({ page });
  await waitForSpell({ page, spellIndex });
};
