import { Page } from "@playwright/test";

export const getSpellSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-item"]`;

  return `[data-testid="spell-item-${spellIndex}"]`;
};

export const getSpellNameSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-name"]`;

  return `[data-testid="spell-name-${spellIndex}"]`;
};

export const waitForSpells = async ({ page }: { page: Page }) => {
  const spellsLocator = page.locator(getSpellSelector());

  await spellsLocator.nth(0).waitFor({ timeout: 10000 });

  return spellsLocator;
};

export const waitForSpell = async ({ page, spellIndex }: { page: Page, spellIndex: string }) => {
  const spellLocator = page.locator(getSpellNameSelector(spellIndex));

  await spellLocator.waitFor({ timeout: 10000 });

  return spellLocator;
};

export const waitForFirstSpell = async ({ page }: { page: Page }) => {
  const spellsLocator = await waitForSpells({ page });

  return spellsLocator.nth(0);
};
