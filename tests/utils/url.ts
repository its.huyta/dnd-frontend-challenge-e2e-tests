import path from 'path';

export const urlJoin = (baseURL: string, ...others: string[]) => {
  return new URL(path.join(...others), baseURL).href;
};
