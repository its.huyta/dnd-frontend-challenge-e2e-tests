import { Page } from "@playwright/test";
import { APP_BASE_URL } from "../../config";
import { urlJoin } from "../utils/url";

export const navigateToSpellListPage = async ({ page }: { page: Page }) => {
  await page.goto(APP_BASE_URL);
}

export const navigateToFavoritePage = async ({ page }: { page: Page }) => {
  await page.goto(urlJoin(APP_BASE_URL, 'favorite'));
}
